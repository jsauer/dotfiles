#!/bin/bash

LOCKSCREEN=$HOME/.config/i3/lockscreen.png
TMP=/tmp/lock_screen.png
EFFECT='boxblur=5:1'
#EFFECT='scale=iw/4:ih/4,scale=4*iw:4*ih:flags=neighbor'

# placement x/y
PX=0
PY=0
# lockscreen image info
R=$(identify $LOCKSCREEN | grep -o '[0-9]*x[0-9]*' | head -1)
RX=$(echo $R | cut -d'x' -f 1)
RY=$(echo $R | cut -d'x' -f 2)

OVERLAY=''
INPUT=''
SR=$(xrandr --query | grep ' connected' | egrep -o '[0-9]+x[0-9]+\+[0-9]+\+[0-9]+')
for RES in $SR
do
	# monitor position/offset
        SRX=$(echo $RES | cut -d'x' -f 1)                   # x pos
        SRY=$(echo $RES | cut -d'x' -f 2 | cut -d'+' -f 1)  # y pos
        SROX=$(echo $RES | cut -d'x' -f 2 | cut -d'+' -f 2) # x offset
        SROY=$(echo $RES | cut -d'x' -f 2 | cut -d'+' -f 3) # y offset
        PX=$(($SROX + $SRX/2 - $RX/2))
        PY=$(($SROY + $SRY/2 - $RY/2))

	INPUT="$INPUT -i $LOCKSCREEN"
	OVERLAY="$OVERLAY,overlay=x=$PX:y=$PY"
done
ffmpeg -loglevel quiet -i <(import -silent -window root png:-) $INPUT \
	-y -filter_complex "$EFFECT$OVERLAY" $TMP
i3lock  -i $TMP
rm $TMP
