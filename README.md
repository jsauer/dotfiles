# Dotfiles

This is my collection of configuration files.

## Usage
Pull the repository and then create the symbolic links with GNU stow

```bash
$ git clone https://gitlab.com/jsauer/dotfiles.git ~/.dotfiles
$ cd ~/.dotfiles
$ stow DIRECTORY
```

## License
[MIT](https://opensource.org/licenses/MIT)
